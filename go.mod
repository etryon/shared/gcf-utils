module gitlab.com/etryon/shared/gcf-utils.git

go 1.16

require (
	cloud.google.com/go v0.103.0 // indirect
	firebase.google.com/go/v4 v4.8.0
	github.com/aws/aws-sdk-go-v2 v1.16.7
	github.com/aws/aws-sdk-go-v2/config v1.15.14
	github.com/aws/aws-sdk-go-v2/credentials v1.12.9
	github.com/aws/aws-sdk-go-v2/service/sts v1.16.9
	github.com/go-playground/validator/v10 v10.9.0
	github.com/golang-jwt/jwt/v4 v4.4.2
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/rs/cors v1.8.2
	github.com/stretchr/testify v1.7.1
	golang.org/x/net v0.0.0-20220708220712-1185a9018129 // indirect
	golang.org/x/oauth2 v0.0.0-20220630143837-2104d58473e0 // indirect
	golang.org/x/sys v0.0.0-20220712014510-0a85c31ab51e // indirect
	google.golang.org/api v0.87.0 // indirect
	google.golang.org/genproto v0.0.0-20220712132514-bdd2acd4974d // indirect
	google.golang.org/grpc v1.48.0 // indirect
)
