package oncall

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"mime"
	"net/http"
	"strings"

	firebase "firebase.google.com/go/v4"
	"firebase.google.com/go/v4/auth"
	"github.com/golang-jwt/jwt/v4"
	"github.com/rs/cors"
)

type Input struct {
	Data *json.RawMessage `json:"data"`
}

type Output struct {
	Result interface{} `json:"result,omitempty"`
	Error  *Error      `json:"error,omitempty"`
}

type Error struct {
	Code    int    `json:"-"`
	Status  string `json:"status"`
	Message string `json:"message"`
}

func (e *Error) Error() string {
	return e.Message
}

func InternalError(message string) *Error {
	return &Error{
		Code:    http.StatusInternalServerError,
		Status:  "INTERNAL",
		Message: message,
	}
}

func PermissionDeniedError(message string) *Error {
	return &Error{
		Code:    http.StatusForbidden,
		Status:  "PERMISSION_DENIED",
		Message: message,
	}
}

func UnauthenticatedError(message string) *Error {
	return &Error{
		Code:    http.StatusUnauthorized,
		Status:  "UNAUTHENTICTED",
		Message: message,
	}
}

func InvalidArgumentError(message string) *Error {
	return &Error{
		Code:    http.StatusBadRequest,
		Status:  "INVALID_ARGUMENT",
		Message: message,
	}
}

func MethodNotAllowedError(message string) *Error {
	return &Error{
		Code:    http.StatusMethodNotAllowed,
		Status:  "INVALID_ARGUMENT",
		Message: message,
	}
}

func UnsupportedMediaTypeError(message string) *Error {
	return &Error{
		Code:    http.StatusUnsupportedMediaType,
		Status:  "INVALID_ARGUMENT",
		Message: message,
	}
}

func DefaultCorsOptions() *cors.Options {
	return &cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{http.MethodPost},
		AllowedHeaders:   []string{"*"},
		AllowCredentials: true,
		MaxAge:           3600,
	}
}

type Options struct {
	CorsOptions   *cors.Options
	TokenVerifier TokenVerifier
}

type Option func(*Options)

func WithAllowedOrigins(origins []string) Option {
	return func(opts *Options) {
		opts.CorsOptions.AllowedOrigins = origins
	}
}

func WithTokenVerifier(tv TokenVerifier) Option {
	return func(opts *Options) {
		opts.TokenVerifier = tv
	}
}

type Handler func(ctx context.Context, token *auth.Token, data *json.RawMessage) (interface{}, error)

func NewHandler(f Handler, options ...Option) http.Handler {
	opts := Options{CorsOptions: DefaultCorsOptions()}
	for _, f := range options {
		f(&opts)
	}
	if opts.TokenVerifier == nil {
		opts.TokenVerifier = FirebaseTokenVerifier()
	}
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var in Input
		err := json.NewDecoder(r.Body).Decode(&in)
		if err != nil {
			log.Printf("ERROR: decoding JSON body: %v", err)
			errorResponse(w, InvalidArgumentError(fmt.Sprintf("malformed message body: %v", err)))
			return
		}
		res, err := f(r.Context(), AuthToken(r), in.Data)
		if err != nil {
			log.Printf("ERROR: handler error: %v", err)
			errorResponse(w, err)
			return
		}
		jsonResponse(w, http.StatusOK, Output{Result: res})
	})

	return cors.New(*opts.CorsOptions).Handler(
		EnforcePostMiddleware(
			EnforceJSONMiddleware(
				BearerAuthMiddleware(opts.TokenVerifier, handler))))
}

// errorResponse returns an HTTP error with a JSON error struct in the output
func errorResponse(w http.ResponseWriter, err error) {
	oncallErr, ok := err.(*Error)
	if !ok {
		log.Printf("ERROR: unexpected internal error: %v", err)
		oncallErr = InternalError("Internal error")
	}
	jsonResponse(w, oncallErr.Code, Output{Error: oncallErr})
}

// jsonResponse writes a JSON response with the given HTTP status code and payload
func jsonResponse(w http.ResponseWriter, code int, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("X-Content-Type-Options", "noniff")
	w.WriteHeader(code)
	enc := json.NewEncoder(w)
	enc.Encode(payload)
}

type TokenVerifier interface {
	VerifyIDToken(context.Context, string) (*auth.Token, error)
}

// FirebaseTokenVerifier creates a Firebase auth client that implements the
// TokenVerifier interface
func FirebaseTokenVerifier() TokenVerifier {
	ctx := context.Background()
	app, err := firebase.NewApp(ctx, nil)
	if err != nil {
		log.Fatalf("ERROR: creating Firebase app: %v", err)
	}
	ac, err := app.Auth(ctx)
	if err != nil {
		log.Fatalf("ERROR: creating Firebase auth client: %v", err)
	}
	return ac
}

type DummyTokenVerifier struct {
	hmacSecret []byte
}

func NewDummyTokenVerifier() *DummyTokenVerifier {
	secret := make([]byte, 16)
	rand.Read(secret)
	return &DummyTokenVerifier{hmacSecret: secret}
}

func (tv *DummyTokenVerifier) MintToken(token *auth.Token) (string, error) {
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"iss": token.Issuer,
		"aud": token.Audience,
		"exp": token.Expires,
		"iat": token.IssuedAt,
		"sub": token.Subject,
		"uid": token.UID,
	})
	return jwtToken.SignedString(tv.hmacSecret)
}

func (tv *DummyTokenVerifier) VerifyIDToken(ctx context.Context, tokenStr string) (*auth.Token, error) {
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		// Validate the algorithm
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		// Return the secret
		return tv.hmacSecret, nil
	})
	if err != nil {
		return nil, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if !(ok && token.Valid) {
		return nil, errors.New("invalid token")
	}
	return &auth.Token{
		Issuer:   claims["iss"].(string),
		Audience: claims["aud"].(string),
		Expires:  int64(claims["exp"].(float64)),
		IssuedAt: int64(claims["iat"].(float64)),
		Subject:  claims["sub"].(string),
		UID:      claims["uid"].(string),
	}, nil
}

type contextKey string

func (c contextKey) String() string {
	return "oncall context key: " + string(c)
}

var contextKeyAuthToken = contextKey("AuthToken")

func BearerAuthMiddleware(tv TokenVerifier, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		const bearerPrefix = "Bearer "
		header := r.Header.Get("Authorization")
		if !strings.HasPrefix(header, bearerPrefix) {
			log.Print("ERROR: missing or malformed authorization header")
			w.Header().Set("WWW-Authenticate", "Bearer")
			errorResponse(w, UnauthenticatedError("Missing or malformed authorization header"))
			return
		}
		idToken := strings.TrimSpace(header[len(bearerPrefix):])
		if len(idToken) == 0 {
			log.Print("ERROR: missing bearer token")
			w.Header().Set("WWW-Authenticate", "Bearer")
			errorResponse(w, UnauthenticatedError("Missing bearer token"))
			return
		}
		bearer, err := tv.VerifyIDToken(r.Context(), idToken)
		if err != nil {
			log.Printf("ERROR: token verification failed: %v", err)
			w.Header().Set("WWW-Authenticate", "Bearer")
			errorResponse(w, UnauthenticatedError("Bearer authentication failed"))
			return
		}
		log.Printf("INFO: authenticated sub: %s, uid: %s, aud: %s", bearer.Subject, bearer.UID, bearer.Audience)
		next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), contextKeyAuthToken, bearer)))
	})
}

func AuthToken(r *http.Request) *auth.Token {
	token, ok := r.Context().Value(contextKeyAuthToken).(*auth.Token)
	if !ok {
		return nil
	}
	return token
}

func EnforcePostMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			log.Printf("ERROR: invalid method: %s", r.Method)
			errorResponse(w, MethodNotAllowedError("Method not allowed"))
			return
		}
		next.ServeHTTP(w, r)
	})
}

func EnforceJSONMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		contentType := r.Header.Get("Content-Type")
		if contentType == "" {
			log.Printf("ERROR: missing Content-Type header")
			errorResponse(w, InvalidArgumentError("Missing Content-Type header"))
			return
		}
		mt, _, err := mime.ParseMediaType(contentType)
		if err != nil {
			log.Printf("ERROR: malformed content type header: %v", err)
			errorResponse(w, InvalidArgumentError("Malformed Content-Type header"))
			return
		}
		if mt != "application/json" {
			log.Printf("ERROR: unsupported media type: %s", mt)
			errorResponse(w, UnsupportedMediaTypeError(fmt.Sprintf("Unsupported media type: %s", mt)))
			return
		}
		next.ServeHTTP(w, r)
	})
}
