package oncall

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"firebase.google.com/go/v4/auth"
	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var tv = NewDummyTokenVerifier()

var validate = validator.New()

var handler = NewHandler(HelloWorld, WithTokenVerifier(tv))

type HelloWorldInput struct {
	Name string `json:"name", validate:"required"`
}

type HelloWorldOutput struct {
	Text string `json:"text"`
}

func HelloWorld(ctx context.Context, token *auth.Token, data *json.RawMessage) (interface{}, error) {
	var in HelloWorldInput
	if err := json.Unmarshal(*data, &in); err != nil {
		return nil, InvalidArgumentError(fmt.Sprintf("malformed input: %v", err))
	}
	if err := validate.Struct(in); err != nil {
		return nil, InvalidArgumentError(err.Error())
	}
	return &HelloWorldOutput{Text: fmt.Sprintf("Hello, %s aka %s!", token.Subject, in.Name)}, nil
}

func OnCallHandler(w http.ResponseWriter, r *http.Request) {
	handler.ServeHTTP(w, r)
}

var validToken = auth.Token{
	Issuer:   "unit-tests",
	IssuedAt: time.Now().Unix(),
	Subject:  "test@example.com",
	Expires:  time.Now().Add(time.Minute * 10).Unix(),
	Audience: "unit-tests",
	UID:      "ccb2cba0-3e4c-11ec-b40e-33e3f165a679",
}

var expiredToken = auth.Token{
	Issuer:   "unit-tests",
	IssuedAt: time.Now().Unix(),
	Subject:  "test@example.com",
	Expires:  time.Now().Add(time.Minute * -10).Unix(),
	Audience: "unit-tests",
	UID:      "718f710a-3e4d-11ec-8f03-3776e739da82",
}

func TestDummyTokenVerifier(t *testing.T) {
	t.Run("Valid Token Test", func(t *testing.T) {
		tokenStr, err := tv.MintToken(&validToken)
		require.NoError(t, err, "Mint token failed unexpectedly")
		assert.NotEmpty(t, tokenStr)
		token, err := tv.VerifyIDToken(context.Background(), tokenStr)
		require.NoError(t, err, "Verify token failed unexpectedly")
		assert.Equal(t, validToken, *token)
	})
	t.Run("Expired token test", func(t *testing.T) {
		tokenStr, err := tv.MintToken(&expiredToken)
		require.NoError(t, err, "Mint token failed unexpectedly")
		assert.NotEmpty(t, tokenStr)
		token, err := tv.VerifyIDToken(context.Background(), tokenStr)
		require.Error(t, err, "Verify expired token succeeded unexpectedly")
		assert.Nil(t, token)
	})
}

func buildBody(x interface{}) []byte {
	in := struct {
		Data interface{} `json:"data"`
	}{x}
	s, err := json.Marshal(in)
	if err != nil {
		panic(err)
	}
	return s
}

type checkResponse func(t *testing.T, res *http.Response)

func assertStatus(code int) checkResponse {
	return func(t *testing.T, res *http.Response) {
		assert.Equal(t, code, res.StatusCode, "Check response status")
	}
}

func assertHeader(header, value string) checkResponse {
	return func(t *testing.T, res *http.Response) {
		assert.Equal(t, value, res.Header.Get(header), "Check response header "+header)
	}
}

func assertError(expect *Error) checkResponse {
	return func(t *testing.T, res *http.Response) {
		var got Output
		err := json.NewDecoder(res.Body).Decode(&got)
		require.NoError(t, err, "Parse error response")
		assert.Equal(t, expect.Status, got.Error.Status)
		if expect.Message != "" {
			assert.Equal(t, expect.Message, got.Error.Message)
		}
	}
}

func assertResult(expect *HelloWorldOutput) checkResponse {
	return func(t *testing.T, res *http.Response) {
		var got struct {
			Result *HelloWorldOutput `json:"result"`
		}
		err := json.NewDecoder(res.Body).Decode(&got)
		require.NoError(t, err, "Parse body")
		require.NotNil(t, got.Result)
		assert.Equal(t, expect, got.Result)
	}
}

func TestHandler(t *testing.T) {
	goodToken, err := tv.MintToken(&validToken)
	require.NoError(t, err, "Create valid token")
	badToken, err := tv.MintToken(&expiredToken)
	require.NoError(t, err, "Create expired token")

	var tests = []struct {
		name    string
		method  string
		headers map[string]string
		body    []byte
		checks  []checkResponse
	}{
		{
			name:    "CORS handler",
			method:  http.MethodOptions,
			headers: map[string]string{"Access-Control-Request-Method": http.MethodPost, "Origin": "localhost"},
			checks: []checkResponse{
				assertStatus(http.StatusNoContent),
				assertHeader("Access-Control-Allow-Credentials", "true"),
				assertHeader("Access-Control-Allow-Origin", "*"),
				assertHeader("Access-Control-Allow-Methods", http.MethodPost),
			},
		},
		{
			name:    "Invalid request method",
			method:  http.MethodGet,
			headers: map[string]string{"Content-Type": "application/json", "Authorization": "Bearer " + goodToken},
			body:    buildBody(HelloWorldInput{Name: "Unit Test"}),
			checks: []checkResponse{
				assertStatus(http.StatusMethodNotAllowed),
				assertError(MethodNotAllowedError("Method not allowed")),
			},
		},
		{
			name:    "Invalid content type",
			method:  http.MethodPost,
			headers: map[string]string{"Content-Type": "image/png", "Authorization": "Bearer " + goodToken},
			checks: []checkResponse{
				assertStatus(http.StatusUnsupportedMediaType),
				assertError(UnsupportedMediaTypeError("Unsupported media type: image/png")),
			},
		},
		{
			name:    "Expired token",
			method:  http.MethodPost,
			headers: map[string]string{"Content-Type": "application/json", "Authorization": "Bearer " + badToken},
			checks: []checkResponse{
				assertStatus(http.StatusUnauthorized),
				assertError(UnauthenticatedError("")),
			},
		},
		{
			name:    "Valid request",
			method:  http.MethodPost,
			headers: map[string]string{"Content-Type": "application/json", "Authorization": "Bearer " + goodToken},
			body:    buildBody(HelloWorldInput{Name: "Unit Test"}),
			checks: []checkResponse{
				assertStatus(http.StatusOK),
				assertResult(&HelloWorldOutput{Text: "Hello, test@example.com aka Unit Test!"}),
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			req := httptest.NewRequest(test.method, "/", bytes.NewReader(test.body))
			for k, v := range test.headers {
				req.Header.Set(k, v)
			}
			rr := httptest.NewRecorder()
			OnCallHandler(rr, req)
			res := rr.Result()
			for _, check := range test.checks {
				check(t, res)
			}
		})
	}
}
