package aws

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials/stscreds"
	"github.com/aws/aws-sdk-go-v2/service/sts"
)

// LoadConfig returns an AWS configuration with temporary credentials for the
// specified role using an id token for the Cloud Function service account.
// The AWS role must be configured with a trust policy that allows this federated
// identity to assume the desired role.
func LoadConfig(region string, audience string, role string) (aws.Config, error) {
	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithRegion(region))
	if err != nil {
		return aws.Config{}, fmt.Errorf("error loading default config: %v", err)
	}
	appCreds := aws.NewCredentialsCache(stscreds.NewWebIdentityRoleProvider(
		sts.NewFromConfig(cfg),
		role,
		&MetadataServiceTokenRetriever{Audience: audience},
	))
	cfg, err = config.LoadDefaultConfig(context.TODO(), config.WithRegion(region), config.WithCredentialsProvider(appCreds))
	if err != nil {
		return aws.Config{}, fmt.Errorf("error loading config with WebIdentityRoleProvider: %v", err)
	}
	return cfg, nil
}

// MetadataServiceTokenRetriever satisfies the stscreds.IdentityTokenRetriever interface.
// It retrieves an identity token for the specified audience from the Google metadata service.
type MetadataServiceTokenRetriever struct {
	Audience string
}

// tokenURL is the URL for retrieving a token from the metadata service
const tokenURL = "http://metadata.google.internal/computeMetadata/v1/instance/service-accounts/default/identity?audience="

// GetIdentityToken retrieves a token for the service account from the metadata service
func (p *MetadataServiceTokenRetriever) GetIdentityToken() ([]byte, error) {
	req, err := http.NewRequest("GET", tokenURL+url.QueryEscape(p.Audience), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Metadata-Flavor", "Google")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return b, nil
}
